var HTMLWebpackPlugin = require('html-webpack-plugin');
var HTMLWebpackPluginConfig = new HTMLWebpackPlugin({
    template: __dirname + '/app/index.html',
    filename: 'index.html',
    inject: 'body'
});
module.exports = {
    mode: 'development',
    entry: __dirname + '/app/index.js',
    devServer: {
        historyApiFallback: true,
      },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            },
            // {
            //     test: /\.(jpe?g|png|gif|svg)$/i,
            //     loaders: [
            //       'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
            //       'image-webpack-loader?bypassOnDebug&optimizationLevel=7&interlaced=false'
            //     ]
            //   } ,
            { 
                test: /\.(png|jpg)$/, 
                loader: 'url-loader' 
            }
        ]
    },
    output: {
        filename: 'transformed.js',
        path: __dirname + '/build',
        publicPath: '/'
    },
    plugins: [HTMLWebpackPluginConfig]
};