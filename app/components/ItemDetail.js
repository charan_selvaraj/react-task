var React = require('react');
var ReactDOM = require('react-dom');
import {useState, useEffect} from 'react';
function ItemDetail({match}){
const [itemDetail, setitemDetail] = useState({
    ratings:{},
    images:{}
});
    useEffect(()=>
        {
            fetchitemDetail();
        },[]);

    const fetchitemDetail = async()=>{
        const response = await fetch(`https://fortnite-api.theapinetwork.com/item/get?id=${match.params.id}`);
        const detail =  await response.json();
        setitemDetail(detail.data.item);
    }
    return (
        <div>
           <h1 style={{padding: "30px"}}>Item Details:</h1>
               <h3>
                   <center>
                       <img src={itemDetail.images.background} alt="" width="300" height="300" /><br></br>
                        <p>Name:&nbsp;<b style={{color:"green"}}>{itemDetail.name}</b></p>
                        <p>Description:&nbsp;<b style={{color:"green"}}>{itemDetail.description}</b></p>
                        <p>Rarity:&nbsp;<b style={{color:"green"}}>{itemDetail.rarity}</b></p>
                        <p>Series:&nbsp;<b style={{color:"green"}}>{itemDetail.series}</b></p>
                        <p>Type:&nbsp;<b style={{color:"green"}}>{itemDetail.type}</b></p>
                        <p>Ratings:&nbsp;<b style={{color:"green"}}>{itemDetail.ratings.totalPoints}</b></p>
                    </center>
               </h3>
        </div>
    );
} 

export default ItemDetail;