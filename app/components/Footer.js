var React = require('react');
var ReactDOM = require('react-dom');
import {Link} from 'react-router-dom';
// import './App.css';
function Footer(){
    const navStyle = {
        display: "flex",
        justifyContent: "space-around",
        // position: "fixed",
        minHeight: "10vh",
        background: "grey",
        color: "black",
        padding: "20px"
    }
    const navlinksStyle = {
        width: "40%",
        display: "flex",
        justifyContent: "center",
        listStyle: "none",
        textDecoration: "none"
    }
    return (
        <nav style={navStyle}>
            <ul style={navlinksStyle}>
                <Link to="/about">
                    <li>About</li>
                </Link>
            </ul>
        </nav>
    );
}
export default Footer;