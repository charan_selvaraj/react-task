var React = require('react');
var ReactDOM = require('react-dom');
import {useState, useEffect} from 'react';
function NavBar(){
    const [menuData, setMenuData] = useState({});
    useEffect(()=>
        {
            fetchMenuData();
        },[]);

    const fetchMenuData = async()=>{
        const menu = await fetch("https://run.mocky.io/v3/c7793513-d3b4-492b-963f-3b3cf62c4b64");
        const menuData = await menu.json();
        setMenuData(menuData);
    }
    const style = {
        float: "left", 
        color: "#f2f2f2",
        textAlign: "center",
        padding: "14px 16px",
        textDecoration: "none",
        fontSize: "17px",
        overflow: "hidden",
        backgroundColor: "#333"
    }
    return (
        <div className="topnav" style={{overflow: "hidden",backgroundColor: "#333"}}>
           <a className="active" style={style} href="#home">{menuData.home}</a>
           <a href="#news" style={style}>{menuData.news}</a>
           <a href="#contact" style={style}>{menuData.contact}</a>
            <a href="#about" style={style}>{menuData.about}</a>
        </div>
    );
}
export default NavBar;