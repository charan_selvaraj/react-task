var React = require('react');
var ReactDOM = require('react-dom');
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'

function Shop(){
const [item, setItem] = useState([]);
    useEffect(()=>
        {
            fetchItems();
        },[]);

    const fetchItems = async()=>{
        const response = await fetch("https://fortnite-api.theapinetwork.com/upcoming/get");
        const items = await response.json();
        setItem(items.data);
    }
    return (
        <React.Fragment>
            <table border="3" style={{padding:"50px", border:"none"}}>
                    <tr>
                        <th>Item Id</th>
                        <th>Name</th>
                    </tr>
                {item.map(i=>(
                    <tr>
                        <td>
                            {i.itemId}
                        </td>
                        <td>
                            <Link to = {`/shop/${i.itemId}`}>{i.item.name}</Link>  
                        </td>
                    </tr>
                ))}
            </table>
        </React.Fragment>
    );
}
export default Shop;