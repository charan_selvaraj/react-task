var React = require('react');
var ReactDOM = require('react-dom');

import Header from './Header.js'
import Footer from './Footer.js'
import Home from './Home.js'
import About from './About.js'
import Shop from './Shop.js'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import ItemDetail from './ItemDetail';
import NavBar from './NavBar';



function App() {
  return (
    <Router>
      <div>
          <Header />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/shop" exact component={Shop} />
          <Route path="/shop/:id" component={ItemDetail} />
        </Switch>
        {/* <Footer /> */}
      </div>
    </Router>

  );
}

export default App;
