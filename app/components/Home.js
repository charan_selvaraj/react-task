var React = require('react');
var ReactDOM = require('react-dom');
import {Row, Col,Button,Card,Container} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCoffee, faEnvelope,faMobileAlt} from '@fortawesome/free-solid-svg-icons';
import { faFacebookF,faTwitter,faLinkedinIn,faYoutube} from '@fortawesome/free-brands-svg-icons';
import {Link} from 'react-router-dom';
import {Fragment,useState, useEffect} from 'react';

function Home(){

  
  
    return (
        <React.Fragment>
          <Row xs={2} md={4} lg={6} className="section2-image" style={{}}>
            <Col></Col>
            <Col className="section2-text">
              <h1 className="section2-title1">
                Find the <span className="section2-title2">Right insurance
                for you & your family</span>
            </h1>
            <p className="section2-content">
                Life throws many unexpected things at all of us. While we usually
                can't stop these things from occuring. we can opt to give our live
                a bit of protection. Insurance is meant to give us some measure of
                protection, at least financially, should a disaster happen There
                are numerous insurance options available, and many financial
                experts tell us that we need to have those insurance policies in 
                place Yet, with so many options, it can be difficult to determine
                what insurance you really need. Purchasing the right insurance is
                always determined by your specific situation Here comes the best
                choice.
            </p>
            </Col>
        </Row>

         <Row xs={2} md={4} lg={6} className="section3-image">
            
            <Col className="section3-text">
            <h1 className="section3-title1">
                Free Quote and <span className="section3-title2">Instant Online Cover</span>
            </h1>
            <p className="section3-content">
                Life throws many unexpected things at all of us. While we usually
                can't stop these things from occuring. we can opt to give our live
                a bit of protection. Insurance is meant to give us some measure of
                protection, at least financially, should a disaster happen There
                are numerous insurance options available, and many financial
                experts tell us that we need to have those insurance policies in 
                place Yet, with so many options, it can be difficult to determine
                what insurance you really need. Purchasing the right insurance is
                always determined by your specific situation Here comes the best
                choice.
            </p>
            </Col>
            <Col>
            </Col>
        </Row>

        <Container>
            <h1 className="card-heading" style={{}}>
              <b>Our Awesome Benefits</b>
            </h1>
        </Container>


        <Row>
          <Col className="card-col1">
            <Card className="card1">
              <Card.Img src={require('../images/card1.png').default} className="card-image1"/>
                <Card.Body>
                <Card.Title className="card-title1"><h4><b>Customer Greetings</b></h4></Card.Title>
                <Card.Text className="card-text1">
                  All of our client care team
                  works in our office ands is an
                  experienced web meeting
                  user ready to help you. Never
                  outsourced.
                </Card.Text>
                </Card.Body>
            </Card>
          </Col>
          
          <Col className="card-col2" >
          <Card className="card2">
            <Card.Img src={require('../images/card2.png').default} className="card-image2"/>
            <Card.Body>
            <Card.Title className="card-title2"><h4><b>Worldwide Access</b></h4></Card.Title>
            <Card.Text className="card-text2">
                  All of our client care team 
                  works in our office ands is an 
                  experienced web meeting
                  user ready to help you. Never
                  outsourced.
            </Card.Text>
            </Card.Body>
          </Card>
          </Col>
          
          <Col className="card-col3">
          <Card className="card3">
            <Card.Img src={require('../images/card3.png').default} className="card-image3"/>
            <Card.Body>
            <Card.Title className="card-title3"><h4><b>Realtime Network</b></h4></Card.Title>
            <Card.Text className="card-text3">
                  All of our client care team 
                  works in our office ands is an 
                  experienced web meeting
                  user ready to help you. Never 
                  outsourced.
            </Card.Text>
            </Card.Body>
          </Card>
          </Col>
        </Row>
        
      <br></br>
      

        <Row>
          <Col className="card-col4">
            <Card className="card4">
              <Card.Img src={require('../images/card4.png').default} className="card-image4"/>
                <Card.Body>
                <Card.Title className="card-title4"><h4><b>Analytics</b></h4></Card.Title>
                <Card.Text className="card-text4">
                  All of our client care team 
                  works in our office ands is an 
                  experienced web meeting
                  user ready to help you. Never 
                  outsourced.
                </Card.Text>
                </Card.Body>
            </Card>
          </Col>
          
          <Col className="card-col5">
          <Card className="card5">
            <Card.Img src={require('../images/card5.png').default} className="card-image5"/>
            <Card.Body>
            <Card.Title className="card-title5"><h4><b>Administration</b></h4></Card.Title>
            <Card.Text className="card-text5">
                  All of our client care team 
                  works in our office ands is an 
                  experienced web meeting
                  user ready to help you. Never 
                  outsourced.
            </Card.Text>
            </Card.Body>
          </Card>
          </Col>

          <Col className="card-col6">
          <Card className="card6">
            <img src={require('../images/card6.png').default} className="card-image6"/>
            <Card.Body>
            <Card.Title className="card-title6"><h4><b>Top Customer Service</b></h4></Card.Title>
            <Card.Text className="card-text6">
                 All of our client care team 
                  works in our office ands is an 
                  experienced web meeting
                  user ready to help you. Never 
                  outsourced.
            </Card.Text>
            </Card.Body>
          </Card>
          </Col>
        </Row>
        </React.Fragment>
    );
}
export default Home;